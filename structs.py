from datetime import datetime
import io

"""
 Data-class that holds data fields related to the HTTP response
"""
class HTTPResponse():
    def __init__(self):
        self.status = 200
        self.headers = []
        self.body = ""

    def __str__(self):
        s = " === HTTP Response === \n" + "-" * 40 + "\n "
        s += "\n ".join( list( map( lambda x: x[0] + ":" + x[1], self.headers ) ))
        s += "\n" + "-" * 40

        if len(self.body) > 0:
            s += str(self.body)
            s += "\n" + "-" * 40
        else:
            s += "\n No body.\n"
        return s

    def __repr__(self):
        if len(self.body) > 30:
            return "<class HTTPResponse> %s:%s..." % (self.status, self.body[0:30])
        else:
            return "<class HTTPResponse> %s:%s..." % (self.status, self.body[0:30])

    def serve(self, req):
        # RFC7231
        # see http://www.iana.org/assignments/http-status-codes/http-status-codes.xhtml
        resp = HTTPResponse()


        resp.headers = [("Content-Type", "text/html; charset=UTF-8"),
                        ("Connection", "close"),
                        ("Cache-Control", "no-cache")
                        ]

        template = """
<html><body>

<div style="width: 100%; position: fixed; top: 30%;">
<h1 style="color:black; text-align:center; font-size: 60pt; margin-top: 5%; margin-bottom:0;"><span style="color:red">&#9888;</span> &nbsp; [prompt]</h1>
<h1 style="color: #555; padding-left: 10%;">[remark]</h1>
</div>

<i style="color: #888; font-size: 14pt; margin: 0.2 0.5in; position: fixed; bottom: 20pt; border-top: #888 solid 1px; width: 89%;padding: 6pt;">
[progname] v. [version]</i>
</body></html>"""

        try:
            prompt = "501 Not Implemented"; remark = "Meanwhile, back at the Justice League...<br>"

            msg = template
            msg = msg.replace('[progname]', GetServerName())
            msg = msg.replace('[version]', GetVersion())
            msg = msg.replace('[prompt]', prompt)
            msg = msg.replace('[remark]', remark)

            resp.body = msg, resp.status = 501
        except:
            pass

        return resp

"""
 Data-class that holds data fields related to the HTTP request
"""
class HTTPRequest():
    def __init__(self):
        self.http_requestline = ""
        self.http_command = ""
        self.headers = ""
        self.body = ""
        self.ipaddr = ""
        self.port = ""
        self.http_version = ""
        self.method = ""
        self.uri = ""
        self.data = b""
        self.server  = None
        self.host    = ""
        self._rstream = None

    def GetData(self):
        if self._rstream is None:
            return

        if "Content-Length" in self.headers:
            contlen = int( self.headers["Content-Length"] )
        elif "Content-length" in self.headers:
            contlen = int( self.headers["Content-length"] )
        else:
            contlen = 0

        if contlen > 0:
            self.data = self._rstream.read(contlen)

        self._rstream = None

        return