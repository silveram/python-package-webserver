import sys

# Python 2 and 3 imports
from json import dumps as JSONEncode, loads as JSONDecode


if sys.version_info[0] > 2:
    # Python 3+ Imports
    from urllib.parse import quote as URIEncode, unquote as URIDecode, urlencode as KeyValEncode, parse_qs as KeyValDecode
    from urllib.parse import urlparse as URLDecompose, urlunparse as URLCompose
    from http.cookies import SimpleCookie as HTTPCookie

else:
    # Python 2  Imports
    from urllib import quote as URIEncode, unquote as URIDecode, urlencode as KeyValEncode
    from urlparse import parse_qs as KeyValDecode, urlparse as URLDecompose, urlunparse as URLCompose
    from Cookie import SimpleCookie as HTTPCookie