import sys, os
from webserver import HTTPResponse
from webserver.util import JSONEncode,URLDecompose, KeyValDecode, URIDecode
from webserver.components import HTTPErrorOutput
import sqlite3
import re

# ------------------------------------------

def _getknowncols( elems ):

    if elems is None:
        return []

    if isinstance(elems, (dict) ):
        proto = elems[ list( elems.keys() )[0] ]
    elif isinstance(elems,(list,tuple,set)):
        proto = elems[ list( elems.keys() )[0] ]
    else:
        proto = elems

    if hasattr( proto,"_dbschema_"):
        knowncols = [ a for a in proto._dbschema_.fields() ]

    else:
        jsontypes = (None, int, float, str, list, dict )
        knowncols = [ a for a in vars(proto) if isinstance(getattr(proto,a), jsontypes ) ]

    return knowncols

# ------------------------------------------

def _htmltable( elems, cols=None, template="[[TABLE]]" ):
    empty_table = "No entries."

    if elems is None or len(elems) < 1:
        return empty_table

    knowncols = _getknowncols(elems)

    if cols is not None and len(cols) > 0:
        cols = [ c for c in cols if c in knowncols ]
    else:
        cols = knowncols

    elems = [ v for k, v in elems.items() if k == v.id ]

    htmlsrc = "<table>\n"

    htmlrow = "</th>\n".join(["        <th name='%s'>%s" % ( c, c.replace("_"," ").title() ) for c in cols])
    htmlrow = "    <tr name='header'>\n" + htmlrow + "</th>\n    </tr>\n"
    htmlsrc += htmlrow

    for i, r in enumerate(elems):
        htmlrow = "</td>\n".join( [ "        <td name='%s'>%s" % ( c, str(getattr(r,c))) for c in cols ] )
        htmlrow = "    <tr name='%s'>\n" % str(i) + htmlrow +"</td>\n    </tr>\n"
        htmlsrc += htmlrow

    htmlsrc += "</table>\n"

    if template is not None:
        htmlsrc = template.replace("[[TABLE]]", htmlsrc )

    return htmlsrc

# ------------------------------------------

def _htmlview( elems, cols=None, template="[[TABLE]]" ):
    empty_table = "No entries."

    if elems is None or len(elems) < 1:
        return empty_table

    knowncols = _getknowncols(elems)

    if cols is not None and len(cols) > 0:
        cols = [ c for c in cols if c in knowncols ]
    else:
        cols = knowncols

    elems = [ v for k, v in elems.items() if k == v.id ]

    htmlsrc = "<table>\n"
    htmlsrc += "</td></tr>\n".join( [ "    <tr><td class='name'>%s</td><td class='value' name='%s'>%s" \
                                       % ( c.replace('_',' ').title(), c, str(getattr(elems[0], c)).replace("\n","<br>\n") ) \
                                           for c in cols ] )
    htmlsrc += "</tr>\n</table>\n"

    if template is not None:
        htmlsrc = template.replace("[[TABLE]]", htmlsrc )

    return htmlsrc

# ------------------------------------------

def _formview( elems, cols=None, template="[[FORM]]" ):

    if elems is None or len(elems) < 1:
        raise RuntimeWarning("No elements match the given key.")

    knowncols = _getknowncols(elems)

    if cols is not None and len(cols) > 0:
        cols = [ c for c in cols if c in knowncols ]
    else:
        cols = knowncols

    chk = [ v for k, v in elems.items() if k == v.id ]

    if len(chk) > 0:
        elems = chk[0]
    else:
        elems = list( elems.items() )[0][1]

    htmlsrc = "<form>\n"

    for c in cols:
        val     = str(getattr(elems, c))
        htmlsrc += "<label>%s</label> " % c.replace('_',' ').title()

        htmlsrc += "<input id='%s' name='%s' type='text' value='%s'></input><br>\n" % ( c, c, val )

    htmlsrc += "</form>\n"

    if template is not None:
        htmlsrc = template.replace("[[FORM]]", htmlsrc )

    return htmlsrc

# ------------------------------------------

def _jsontable( elems, cols=None ):
    empty_table = "{}"

    if elems is None or len(elems) < 1:
        return empty_table

    knowncols = _getknowncols(elems)

    if cols is not None and len(cols) > 0:
        cols = [ c for c in cols if c in knowncols ]
    else:
        cols = knowncols

    elems = [ v for k, v in elems.items() if k == v.id ]
    jsonelems = [ JSONEncode( dict( [ ( c, getattr(r,c) ) for c in cols ]  ) ) for r in elems ]

    return "[" + ", ".join( jsonelems ) + "]"


# ------------------------------------------

def getrequestSQL(pars, altkeyfield=None ):

    where="";
    orderby="ORDER BY `id` ASC";
    limit="LIMIT 100"; offset=""
    int_limit = 100

    if ( "key" not in pars ):
        pass
    else:
        key = ''.join( [ c for c in pars["key"] if re.search("([a-z]|[A-Z]|[0-9]|[-_*%? ])", c) ] )
        where="WHERE ( `id` GLOB '%s' " % ( key, )

        if altkeyfield is not None:
            where += "OR `%s` GLOB '%s' )" % ( altkeyfield, key )
        else:
            where += ")"

    if ("limit" in pars ):
        int_limit = pars["limit"]
        limit = "LIMIT %i" % int_limit

    if ("page" in pars ):
        offset = int_limit * ( pars["page"] - 1 )
        offset = " OFFSET %i" % offset

    if "query" in pars:

        for k,v in pars["query"].items():
            try:
                if re.match("^([a-z]|[A-Z]|[0-9]|[-_*% ])*$", k) is not None and \
                    re.match("^([a-z]|[A-Z]|[0-9]|[-_*% ])*$", v[0]) is not None:

                    if v[0].lower() != 'null':
                        where += " AND `%s` GLOB '%s'" % ( k, v[0] )
                    else:
                        where += " AND `%s` IS NULL" % (k)
            except Exception:
                pass


    constraints = "%s %s %s %s" % ( where, orderby, limit, offset )


    return constraints


# ------------------------------------------

def dbview_decorator(dbpath):

    def decorator(func):
        def wrapper(*args):
            try:
                conn = sqlite3.Connection(dbpath)
                req = args[0]
                urlparts = URLDecompose(req.uri)
                query    = KeyValDecode(urlparts.query)

                print(urlparts, query)

                pars = {}
                if len(args) <= 1:
                    pars["format"] = "json"

                elif len(args) > 1:

                    if re.search("[.]htm[l]?$", urlparts.path, re.I ):
                        pars["format"] = "html"

                    elif re.search("htm[l]?$", urlparts.path, re.I ):
                        pars["format"] = "html"
                    elif re.search("[.]form$", urlparts.path, re.I ):
                        pars["format"] = "form"
                    elif re.search("form$", urlparts.path, re.I ):
                        pars["format"] = "form"
                    else:
                        pars["format"] = "json"

                    keyname = re.search( "^[^.][^.]*", os.path.basename(urlparts.path) )

                    if keyname and keyname.group(0).lower() not in ["html","json","","form"]:
                        pars["key"] = URIDecode( keyname.group(0) )


                    # pagination limit
                    try:
                        limit = [ v for k,v in query.items() if k.lower() == 'limit' ]

                        if len(limit) > 0:
                            limit = int( limit[0][0] )
                            if limit > 1000: limit = 1000
                            if limit < 1:  limit = 1
                            pars["limit"] = limit
                    except Exception:
                        pass

                    # page number
                    try:
                        page = [v for k, v in query.items() if k.lower() == 'page']

                        if len(page) > 0:
                            page = int(page[0][0])
                            if page < 1:  page = 1
                            pars["page"] = page
                    except Exception:
                        pass

                    # orderby
                    try:
                        orderby = [v for k, v in query.items() if k.lower() == 'orderby']

                        if len(page) > 0:
                            pars["orderby"] = orderby

                    except Exception:
                        pass

                    pars["query"] = dict( [ (k,v) for k,v in query.items() if k.lower() not in ["limit","page","orderby"] ] )

                else:
                    pass

                data, cols, template = func(conn, pars )

                resp = HTTPResponse(); resp.status = 200

                if pars["format"] == "json":
                    resp.headers = [('Content-Type','application/json')]
                    resp.body = _jsontable( data, cols=cols )

                elif pars["format"] == 'html' and len(data) > 1:
                    resp.headers = [('Content-Type','text/html')]
                    resp.body = _htmltable( data, cols=cols, template=template )

                elif pars["format"] == 'html' and len(data) == 1:
                    resp.headers = [('Content-Type','text/html')]
                    resp.body = _htmlview( data, cols=cols, template=template )

                elif pars["format"] == 'html' and len(data) == 0:
                    return HTTPErrorOutput(400)

                elif pars["format"] == 'form' and len(data) > 1:
                    resp.headers = [('Content-Type','text/html')]
                    resp.body = _htmltable( data, cols=cols, template=template )

                elif pars["format"] == 'form' and len(data) <= 1:
                    resp.headers = [('Content-Type','text/html')]
                    resp.body = _formview( data, cols=cols, template=template )

                else:
                    raise RuntimeError("Unknown format")

            except Exception:
                conn.close()
                raise

            return resp

        return wrapper
    return decorator