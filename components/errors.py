import sys, traceback

from .. import HTTPResponse
from .. import GetServerName, GetVersion


def ExceptionOutput(ex, modulename="", headline="&#128027; Yikes!"):
    resp = HTTPResponse()

    resp.status = 500

    resp.headers = [("Content-Type", "text/html; charset=UTF-8"),
                    ("Connection", "close"),
                    ("Cache-Control", "no-cache")
                    ]

    template = """
<html>
<div style="width: 100%;">
<h1 style="color:#222; text-align:center; font-size: 60pt;">[headline]</h1>
<h1 style="color:#C44; padding-left: 10%;">An server error occurred during this request:</h1>
<h1 style="color: #555; padding-left: 12%;">[error]</h1>
</div>

<pre style="margin-left: 5%; margin-right: 5%; padding: 15pt 3% 15pt 3%;
        border-left: maroon 5px solid; background: #FFCCCC;">
[traceback]
</pre>

<i style="color: #888; font-size: 14pt; margin: 0.2 0.5in; position: fixed; bottom: 20pt; border-top: #888 solid 1px; width: 89%;padding: 6pt;">
[progname] v. [version] [module]</i>
</html>
"""

    try:
        exc_type, exc_value, exc_traceback = sys.exc_info()

        if len(modulename) > 0:
            modulename = " -- " + modulename

        msg = template
        msg = msg.replace('[progname]', GetServerName())
        msg = msg.replace('[version]', GetVersion())
        msg = msg.replace('[module]', modulename)
        msg = msg.replace('[headline]', headline)

        msg = msg.replace('[error]', "%s: %s" % (repr(ex.__class__.__name__).replace("'", ""), str(ex)))

        tb = repr(traceback.format_exception(exc_type, exc_value, exc_traceback)).replace('\\n', '\n')
        tb = tb.replace('[', '').replace(']', '')
        tb = tb.replace("', ", '').replace('"', '').replace("'", '')

        msg = msg.replace('[traceback]', tb)

        resp.headers.append(("Content-Length", len(msg.encode(encoding="UTF-8"))))
        resp.body = msg

    except Exception as ex:
        raise

    return resp


def HTTPErrorOutput(code):
    # RFC7231
    # see http://www.iana.org/assignments/http-status-codes/http-status-codes.xhtml
    std_codes = {
        400: 'Bad Request',
        401: 'Unauthorized',
        402: 'Payment Required',
        403: 'Forbidden',
        404: 'Not Found',
        405: 'Method Not Allowed',
        406: 'Not Acceptable',
        407: 'Proxy Authentication Required',
        408: 'Request Timeout',
        409: 'Conflict',
        410: 'Gone',
        411: 'Length Required',
        412: 'Precondition Failed',
        413: 'Payload Too Large',
        414: 'URI Too Long',
        415: 'Unsupported Media Type',
        416: 'Range Not Satisfiable',
        417: 'Expectation Failed',
        421: 'Misdirected Request',
        422: 'Unprocessable Entity',
        423: 'Locked',
        424: 'Failed Dependency',
        425: 'Unassigned',
        426: 'Upgrade Required',
        428: 'Precondition Required',
        429: 'Too Many Requests',
        431: 'Request Header Fields Too Large',
        500: 'Internal Server Error',
        501: 'Not Implemented',
        502: 'Bad Gateway',
        503: 'Service Unavailable',
        504: 'Gateway Timeout',
        505: 'HTTP Version Not Supported',
        506: 'Variant Also Negotiates',
        507: 'Insufficient Storage',
        508: 'Loop Detected',
        510: 'Not Extended',
        511: 'Network Authentication Required'
    }

    resp = HTTPResponse()

    # convert to integer
    try:
        code = int(code)
    except:
        code = 0

    if code in std_codes:
        prompt = std_codes[code]
    else:
        code = 400
        prompt = std_codes[400]

    remark = "Not everyone can be perfect all the time..."

    if code == 400 or code == 405 or code == 502 or code == 503:
        remark = "If you'd like to make a call, please hang up and try again..."

    if code == 404 or code == 501 or code == 503:
        remark = "It doesn't look like we are in Kansas anymore..."

    if code == 401:
        remark = "You look so familiar too..."

    if code == 403:
        remark = "I'm sorry, Dave. I'm afraid I can't do that."

    if code == 410:
        remark = "The cake is a lie!"

    resp.headers = [("Content-Type", "text/html; charset=UTF-8"),
                    ("Connection", "close"),
                    ("Cache-Control", "no-cache")
                    ]

    template = """
<html><body>

<div style="width: 100%; position: fixed; top: 30%;">
<h1 style="color:black; text-align:center; font-size: 60pt; margin-top: 5%; margin-bottom:0;"><span style="color:red">&#9888;</span> &nbsp; [prompt]</h1>
<h1 style="color: #555; padding-left: 10%;">[remark]</h1>
</div>

<i style="color: #888; font-size: 14pt; margin: 0.2 0.5in; position: fixed; bottom: 20pt; border-top: #888 solid 1px; width: 89%;padding: 6pt;">
[progname] v. [version]</i>

</body></html>"""

    try:
        msg = template
        msg = msg.replace('[progname]', GetServerName())
        msg = msg.replace('[version]', GetVersion())
        msg = msg.replace('[prompt]', "%s %s" % (code, prompt))
        msg = msg.replace('[remark]', remark)

        resp.headers.append(("Content-Length", len(msg.encode(encoding="UTF-8"))))
        resp.body = msg
        resp.status = code
    except:
        pass

    return resp


def ErrorOutput(ex, modulename="", headline="Oops!"):
    resp = HTTPResponse()

    resp.status = 500

    resp.headers = [("Content-Type", "text/html; charset=UTF-8"),
                    ("Connection", "close"),
                    ("Cache-Control", "no-cache")
                    ]

    template = """
<html><body>

<div style="width: 100%; position: fixed; top: 20%;">
<h1 style="color:#222; text-align:center; font-size: 60pt;">[headline]</h1>
<h1 style="color:#C44; padding-left: 10%;">An unexpected condition occurred during this request:</h1>
<h1 style="color: #555; padding-left: 12%;">[prompt]</h1>
</div>

<i style="color: #888; font-size: 14pt; margin: 0.2 0.5in; position: fixed; bottom: 20pt; border-top: #888 solid 1px; width: 89%;padding: 6pt;">
[progname] v. [version] [module]</i>

</body></html>"""

    try:
        exc_type, exc_value, exc_traceback = sys.exc_info()

        if len(modulename) > 0:
            modulename = " -- " + modulename

        msg = template
        msg = msg.replace('[progname]', GetServerName())
        msg = msg.replace('[version]', GetVersion())
        msg = msg.replace('[module]', modulename)
        msg = msg.replace('[headline]', headline)

        msg = msg.replace('[prompt]', "%s" % (str(ex).replace("\n", "<br>")))

        resp.headers.append(("Content-Length", len(msg.encode(encoding="UTF-8"))))
        resp.body = msg

    except Exception:
        pass

    return resp