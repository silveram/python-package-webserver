import sys, _io
import datetime
import threading
import time
import socket


from .server import HTTPServer

class HTTPServerThread( threading.Thread ):
    def __init__(self, i, switchboard, socket, port, logfile, allowedmethods, proc_no = None ):
        threading.Thread.__init__(self)
        self.instance = i
        self.daemon  = True

        self.httpd = HTTPServer(switchboard, 0, logfile, allowedmethods)
        self.httpd.socket = socket
        self.httpd.server_bind  = lambda self: None   # override default action
        self.httpd.server_close = lambda self: None   # override default action
        self.httpd.server_port = port

        if proc_no is not None:
            self.httpd.server_name += "[ PID %i Thread %i]" % (proc_no, i)
        else:
            self.httpd.server_name += "[ Thread %i]" % i


    def run(self):
        self.httpd.serve_forever()

# ----------------------------------------------------------------------------------------

class ThreadedHTTPServer():
    """
    Based on example presented at:
    https://stackoverflow.com/questions/46210672/python-2-7-streaming-http-server-supporting-multiple-connections-on-one-port
    """

    def __init__(self, switchboard, port=0,
                                    logfile=sys.stderr,
                                    allowedmethods = [ "GET", "POST" ],
                                    threadcount=12, socket=None,
                                    proc_no = None
                 ):

        # setup the socket

        if socket is None:
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self.socket.bind(('',port))
            self.socket.listen(5)
        else:
            self.socket = socket

        # setup the logfile
        if isinstance(logfile, _io.TextIOWrapper):
            self._logfile = logfile.buffer
        else:
            self._logfile = logfile

        self.scheme = "http"
        self.allowed_methods = allowedmethods
        self.switchboard = switchboard

        self.threads = []

        self.server_port = port

        for i in range(threadcount):
            self.threads.append( HTTPServerThread( i, self.switchboard, self.socket, port, logfile, allowedmethods, proc_no=proc_no ) )

        self.server_name = self.threads[0].httpd.server_name


        self.log("Multi-threaded N=%s Server Started." % threadcount )
    # -------------------------------------

    def log(self, message):
        prefix = b"%s %s:%i - " % (str(datetime.datetime.now()).encode(encoding='ascii', errors='ignore'),
                                   self.server_name.encode(encoding='ascii', errors='ignore'),
                                   self.server_port)

        # convert to a byte stream in a python 2 / 3 safe manner
        if isinstance(message, type(u"a")) == True:
            message = message.encode(encoding='ascii', errors='ignore')
        elif isinstance(message, bytes):
            pass
        else:
            message = repr(message).encode(encoding='ascii', error='ignore')

        self._logfile.write(prefix + message + b'\n')

    def serve_forever(self):

        try:
            for t in self.threads:
                t.start()

            while True:
                time.sleep(3600)

        except KeyboardInterrupt as ex:
            for t in self.threads:
                del(t)

            self.__del__()

        except Exception as ex:
            raise

    def __del__(self):
        # a not eloquant way of trying but not requiring a log entry and closing the log file if still open
        try:
            self.log("Multi-threaded Server Stopped.")
        except:
            pass

        try:
            self._logfile.close()
        except:
            pass