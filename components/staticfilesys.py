from .. import HTTPResponse
from .  import HTTPErrorOutput, ExceptionOutput, ErrorOutput
import os, os.path
from os.path import exists as pathexists, abspath
import threading
import re
import mimetypes
from hashlib import md5 as hashgen


STATIC_FILE_DEFAULT_WHITELIST = [ ".*[/][^./]*[.](htm|html|xml|css|js|json)$",
                                  ".*[/][^./]*[.](png|jpg|jpeg|gif)$",
                                  ".*[/][^./]*[.](txt|md|pdf|csv|tsv)$",
                                  ".*[/][^./]*[.](doc|ppt|xls)[a-z]*$",
                                  ".*[/][^./]*[.](tar|tar.gz)$",
                                  ".*[/][^./]*$",
                                  ".*[/]favicon[.]ico$"
                                ]

STATIC_FILE_DEFAULT_BLACKLIST = [ "[/][.]git", "[/][.]", "[/][_][_].*[_][_][.]" ]


class StaticFileSystem():

    def __init__( self, localpath = "",
                        recursive = True,
                        whitelist = STATIC_FILE_DEFAULT_WHITELIST,
                        blacklist = STATIC_FILE_DEFAULT_BLACKLIST,
                        mappingrules = None   ):

        self._cache_files = {}        # records state of whitelisted files
        self._cache_map   = {}        # records previous routes
        self._cache_regex = {}        # records compiled regex

        self.localrootdir = localpath
        self.whitelist    = whitelist
        self.blacklist    = blacklist

        self.mappingrules = None

        if mappingrules is None:
            self.mappingrules = []
        else:
            self.mappingrules = mappingrules

        self.recursive    = recursive

        self.threadLock   = threading.Lock()
        self.threadLock2  = threading.Lock()

        self.rebuildCache()

    # ------------------------------------------------------------------------

    def rebuildCache(self):
        self._cache_files = {}        # records state of whitelisted files
        self._cache_map   = {}        # records previous routes
        self._cache_regex = {}        # records compiled regex
        self._cache_etags = {}

        self._BuildRegexCache()
        self._cache_files = self._FileWhitelisted( _getfilelist(self.localrootdir, self.recursive) )

    # ------------------------------------------------------------------------

    def _BuildRegexCache(self):

        if self.threadLock.locked(): return

        self.threadLock.acquire()

        for wl_exp in self.whitelist:
            if not wl_exp in self._cache_regex:
                self._cache_regex[ wl_exp ] = re.compile( wl_exp, re.IGNORECASE)

        for bl_exp in self.blacklist:
            if not bl_exp in self._cache_regex:
                self._cache_regex[ bl_exp ] = re.compile( bl_exp, re.IGNORECASE)

        for rule in self.mappingrules:
            if not rule[0] in self._cache_regex:
                self._cache_regex[rule[0]] = re.compile( rule[0], re.IGNORECASE)

        self.threadLock.release()

    # ------------------------------------------------------------------------

    def _FileWhitelisted(self, filelst):

        if isinstance(filelst, (list, tuple)):

            tabout = {}

            for curfile in filelst:
                wl_chk = len( [ x for x in self.whitelist if self._cache_regex[x].match(curfile)  ] ) > 0
                bl_chk = len( [ x for x in self.blacklist if self._cache_regex[x].search(curfile) ] ) < 1
                tabout[curfile] = wl_chk and bl_chk

            return tabout

        else:
            wl_chk = len( [x for x in self.whitelist if self._cache_regex[x].match( filelst ) ]) > 0
            bl_chk = len( [x for x in self.blacklist if self._cache_regex[x].search( filelst) ]) < 1

            return wl_chk and bl_chk

    # -----------------------------------------------------------------

    def _MapURI(self, uri ):

        for curRegEx, curSubst in self.mappingrules:
            curRegEx = self._cache_regex[curRegEx]
            uri = curRegEx.sub(curSubst, uri )

        return uri

    # -----------------------------------------------------------------

    def ProcessRequest( self, req ):

        resp = HTTPResponse()

        # look up the mapped_path based on the mapping rules
        if req.uri in self._cache_map:
            mapped_path = self._cache_map[req.uri]
        else:
            mapped_path = self._MapURI(req.uri)

            if not self.threadLock2.locked():
                self.threadLock2.acquire()
                self._cache_map[req.uri] = mapped_path
                self.threadLock2.release()

        if mapped_path[0] == "/":
            mapped_path[1:]

        mapped_path = self.localrootdir + "/" + mapped_path

        chk_exists = os.path.exists(mapped_path) and os.path.isfile(os.path.realpath(mapped_path))

        if mapped_path in self._cache_files:
            # cached result
            if not self._cache_files[mapped_path]:
                return HTTPErrorOutput(403)

            if not chk_exists: return HTTPErrorOutput(404)

        else:
            # not cached result
            if not self.threadLock2.locked():
                self.threadLock2.acquire()
                self._cache_map[mapped_path] = self._FileWhitelisted(mapped_path)
                self.threadLock2.release()

            if not self._FileWhitelisted(mapped_path):
                return HTTPErrorOutput(403)

            if not chk_exists: return HTTPErrorOutput(404)

       # file exists and is whitelisted
        fstat    = os.stat(mapped_path)
        statestr = " ".join( [ mapped_path, str(fstat.st_size), str(fstat.st_mtime) ] )
        eTag = '"' + hashgen( statestr.encode() ).hexdigest() + '"'

        resp.status = 200
        FP = open(mapped_path,'rb'); filedata = FP.read(); FP.close()
        resp.body   = filedata

        resp.headers = [ ("Content-Type", mimetypes.guess_type( mapped_path )[0]),
                         ("Cache-Control", "public, max-age=259200"),
                         ("ETag", eTag )
                       ]

        return resp

    # -----------------------------------------------------------------

    def _ServeFile( localpath ):

        resp = HTTPResponse()

        resp.status = 200
        resp.headers = []

        try:
            FP = open( localpath, "rb" )
            resp.body = FP.read()
            FP.close()

        except Exception as ex:
            ExceptionOutput(ex)

            try:
                FP.close()
            except:
                pass

        return resp

def _getfilelist(path, recursive=True):

        filelist = []

        path = abspath(path)

        if path[-1] == '/':
            path = path[:-1]

        # recursive - find in subdirectories
        if (recursive == True):
            for dirname, subdirs, files in os.walk(path, followlinks=True ):
                for f in files:
                    dirname.replace('./', path + '/', 1)
                    filelist.append(dirname + '/' + f)

        # non-recursive - just files
        else:
            filelist = [ path + '/' + f for f in os.listdir(path) if os.path.isfile(os.path.realpath(f)) and f[0] != '.' ]

        # return the list of files
        return filelist