import sys

# Python 2 / 3 Imports

if sys.version_info[0] > 2:
    # Python 3+ Imports
    from http.server import BaseHTTPRequestHandler

else:
    # Python 2  Imports
    from BaseHTTPServer import BaseHTTPRequestHandler


from .structs import HTTPRequest, HTTPResponse

class HTTPHandler(BaseHTTPRequestHandler):
    def __init__(self, request, client_address, server):

        # here we use the 'my' prefix to prevent any collisions
        self.myResponse = HTTPResponse()
        self.myRequest  = HTTPRequest()

        # set default behavior
        self.myResponse.status = 400
        self.myResponse.headers = ["Content-Type", "text/html"]
        self.myResponse.body = "Nothing to show for the given URI."

        for m in server.allowed_methods:
            setattr(self, "do_%s" % m.upper(), self._switchboard_dispatch )

        BaseHTTPRequestHandler.__init__(self, request, client_address, server)



    def _switchboard_dispatch(self):

        # build request structure
        self.myRequest.headers = self.headers
        self.myRequest.http_command = self.command
        self.myRequest.http_requestline = self.requestline
        self.myRequest.ipaddr = self.client_address[0]
        self.myRequest.port   = self.client_address[1]
        self.myRequest.method = self.command
        self.myRequest.http_version = self.protocol_version
        self.myRequest.uri    = self.path
        self.myRequest.server = self.server
        self.myRequest._rstream  = self.rfile
        self.myRequest.host = [ x[1] for x in self.headers.items() if x[0].lower() == 'host' ]

        if len( self.myRequest.host) > 0:
            self.myRequest.host = self.myRequest.host[0]
        else:
            self.myRequest.host = None

        # forward
        resp = self.server.switchboard(self.myRequest)

        if not resp is None:
            self.myResponse = resp

        self._sendtoclient()

    def log_message(self, format, *args):
        self.server.log( "Request from %s:%i %s" % (self.client_address[0], self.client_address[1], format%args) )


    def _sendtoclient(self):

        self.send_response(self.myResponse.status)

        # convert to a list of tuples if a dictionary
        if isinstance(self.myResponse.headers, dict):
            self.myResponse.headers = dict(self.myResponse.headers).items()

        text_format = True

        # convert body to bytes
        if isinstance( self.myResponse.body, type( u"a" ) ) == True:
            self.myResponse.body =  self.myResponse.body.encode(encoding='utf-8', errors='ignore')
        elif isinstance(self.myResponse.body, bytes):
            text_format = False
            pass
        else:
            self.myResponse.body = repr(self.myResponse.body).encode(encoding='utf-8', errors='ignore')


        # check if there is a Content-Length header:
        if len(self.myResponse.body) > 0:

            if len( list( filter( lambda x: x[0].lower() == "content-length", self.myResponse.headers ) ) ) < 1:
                self.myResponse.headers.append( ("Content-Length", "%i" % len(self.myResponse.body) ) )

        # write headers
        if len(self.myResponse.headers) > 0:
            for hdr in self.myResponse.headers:
                self.send_header(hdr[0], hdr[1])
            self.end_headers()

        # write body
        self.wfile.write( self.myResponse.body )

        # ensure there is a blank line to prevent the client from hanging in case the content-length isn't specified
        # or the message didn't contain a blank line

        # Note: this causes an error for binary data types, as the browser appends it to the byte stream

        if text_format:
            self.wfile.write(b"\r\n")

        return