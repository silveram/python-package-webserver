import sys, _io
import datetime

# Python 2 / 3 Imports

if sys.version_info[0] > 2:
    # Python 3+ Imports
    from http.server import HTTPServer as BaseHTTPServer, BaseHTTPRequestHandler

else:
    # Python 2  Imports
    import BaseHTTPServer

# Python 2 / 3 Imports

if sys.version_info[0] > 2:
    # Python 3+ Imports
    from http.server import HTTPServer as BaseHTTPServer, BaseHTTPRequestHandler

else:
    # Python 2  Imports
    from BaseHTTPServer import HTTPServer as BaseHTTPServer

from .handler import HTTPHandler

# ---------------------------------------------------------------------------------------------

class HTTPServer( BaseHTTPServer ):
    def __init__(self, switchboard, port=0,
                                    logfile=sys.stderr,
                                    allowedmethods = [ "GET", "POST" ]):

        BaseHTTPServer.__init__(self, ('', port), HTTPHandler)

        if isinstance(logfile, _io.TextIOWrapper):
            self._logfile = logfile.buffer
        else:
            self._logfile = logfile

        self.scheme = "http"
        self.allowed_methods = allowedmethods
        self.switchboard = switchboard

        self.log("Server Started.")

    def log(self, message):
        prefix = b"%s %s:%i - " % (str(datetime.datetime.now()).encode(encoding='ascii', errors='ignore'),
                                   self.server_name.encode(encoding='ascii', errors='ignore'),
                                   self.server_port)

        # convert to a byte stream in a python 2 / 3 safe manner
        if isinstance(message, type(u"a")) == True:
            message = message.encode(encoding='ascii', errors='ignore')
        elif isinstance(message, bytes):
            pass
        else:
            message = repr(message).encode(encoding='ascii', error='ignore')

        self._logfile.write(prefix + message + b'\n')
        self._logfile.flush()

    def __del__(self):
        # a not eloquant way of trying but not requiring a log entry and closing the log file if still open
        try:
            self.log("Server Stopped.")
        except:
            pass

        try:
            self._logfile.close()
        except:
            pass

