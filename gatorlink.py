import sys
from .util import *
import re

# Python 2 / 3 Imports

if sys.version_info[0] > 2:
    # Python 3+ Imports
    import http.client as httpclient

else:
    # Python 2  Imports
    import httplib as httpclient



# This is URL to bind this service with
# preferably, the location has little content, since the content will be ignored.
SHIB_PING_URL = "https://internal.phhp.ufl.edu/idmd/"

# This is the reported user agent
SERVICE_AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:58.0) Gecko/20100101 Firefox/58.0"

# These are the basic request headers
REQ_HDRS_BASE = { "User-Agent": SERVICE_AGENT,
                  "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                  "Accept-Language": "en-US,en;q=0.5",
                  "Accept-Encoding": "identity",
                  "Connection": "keep-alive",
                  "Upgrade-Insecure-Requests": "1"
                }


class GatorlinkError(Exception):
       pass


# This is a wrapper that handles low-level HTTP requests
# It keeps a persistent connection if the server requests Keep-Alive
# It returns the response structure and a tuple containing the connection (or None ), the Cookie and Location state

def _HTTPGETREQ( URL, headers, cookie = None, connection = None, debug = True ):

    urlParts = URLDecompose( URL )
    
    # Set the request cookie
    for h in filter( lambda x: x[0].lower() == 'cookie', headers.items() ):
        headers.pop(h)

    if not cookie is None:
            headers["Cookie"] = cookie.output(header='')

    # setup connection if none
    if connection is None:
        if urlParts.scheme == "https":
            connection = httpclient.HTTPSConnection( urlParts.hostname, urlParts.port)
        else:
            connection = httpclient.HTTPConnection( urlParts.hostname, urlParts.port)


    reqstr = URLCompose( ("","", urlParts[2], urlParts[3], urlParts[4], urlParts[5] ) )

    if len(reqstr) < 1:
        reqstr = "/"

    if debug == True:
        print
        "> [%s] %s" % ("GET", reqstr)

        for h in headers.items():
            print("> %s: %s" % (h[0], h[1]) )

        print ("-" * 50)

    # form get request
    connection.putrequest( "GET", reqstr )

    for h in headers.keys():
        connection.putheader( h, headers[h] )
    connection.endheaders()

    resp = connection.getresponse()


    # update cookie and location information if provided
    resp_cookies = list( filter( lambda x: x[0].lower() == 'set-cookie', resp.getheaders() )  )

    if len(resp_cookies) > 0 and cookie is None:
        cookie = HTTPCookie()

    for c in resp_cookies:
        cookie.load(c[1])


    if resp.status >= 300 and resp.status < 400:
        loc = list( filter( lambda x: x[0].lower() == 'location', resp.getheaders() ) )

        if len(loc) > 0:
            URL = loc[0][1]


    if debug == True:
        print
        "< [%s] %s" % (resp.status, resp.reason)

        for h in resp.getheaders():
            print("< %s: %s" % ( h[0], h[1]) )

        print ("-" * 50)


    return ( resp, ( connection, cookie, URL ) )


class GatorlinkAuthRequestToken():
    def __init__(self):
        self.host      = ""
        self.path      = ""
        self.sessionid = ""
        self.authurl   = ""
        self.authtoken = ""


def GetAuthRequestToken():

    # set initial state
    URL        = SHIB_PING_URL
    headers    = REQ_HDRS_BASE.copy()

    reqtoken      = GatorlinkAuthRequestToken()
    reqtoken.host = URLDecompose(URL).hostname
    reqtoken.path = URLDecompose(URL).path

    try:
        # naive ping to the resource
        resp, state = _HTTPGETREQ( URL, headers, cookie=None, connection=None )


        if resp.status != 302:
            raise GatorlinkError("Unexpected response status %s returned by resource request (Status 302 Expected)" % resp.status)

        # unpack state tuple
        conn_resource, cookie_resource, URL = state
        conn_resource.close()

        # get session token from authentication server
        resp, state = _HTTPGETREQ(URL, headers, cookie=None, connection=None)
        resp.read()

        if resp.status != 302:
            raise GatorlinkError("Unexpected response status %s returned during session token request (Status 302 Expected)" % resp.status )

        conn_idp, cookie_idp, URL = state


        resp, state = _HTTPGETREQ(URL, headers, connection=conn_idp, cookie=cookie_idp )

        if resp.status != 200:
            raise GatorlinkError(
                "Unexpected response status %s returned during session token request (Status 200 Expected)" % resp.status )

        body = resp.read()


        login_data = _PARSE_GATORLINK_LOGINPAGE(body.decode())
        conn_idp.close()


    except GatorlinkError as ex:
        raise GatorlinkError(repr(ex))

    except Exception as ex:
        raise GatorlinkError("An exception occured: %s" % repr(ex) )

    print()
    return

"""
Do a little REGEX magic on the HTML Gatorlink Login Page to Obtain the Destination and 
form parameters. 

Return is a tuple, postdestination and postdata
"""
def _PARSE_GATORLINK_LOGINPAGE( html ):

    postdest = ""
    re_res = re.search("<.?form.?action.?[\"']([^\"']*)[^>]*[>]", html)

    if not re_res is None:
        postdest = str(re_res.group(1))


    postdata = {}

    find = 0
    while find >= 0:
        re_res  = re.search( "<[^/i]*input[^>]*[>]", html[find:] )

        if re_res is None:
            find = -1
        else:
            re_res_name = re.search("name=[^\"']*[\"']([^\"']*)[\"']", str(re_res.group(0)) )
            re_res_value = re.search("value=[^\"']*[\"']([^\"']*)[\"']", str(re_res.group(0)) )

            if re_res_name is not None and re_res_value is not None:
                postdata[ str(re_res_name.group(1)) ] = str(re_res_value.group(1))

            find += re_res.span(0)[1]

    return ( postdest, postdata )

def test():
    GetAuthRequestToken()
    pass
