from .server  import HTTPServer
from .threadedserver import ThreadedHTTPServer, HTTPServerThread
from .structs import HTTPRequest, HTTPResponse
from .util import *

SERVER_NAME = "python-package-webserver"
VERSION     = "0.1"

def GetServerName():
    global SERVER_NAME; return SERVER_NAME

def GetVersion():
    global VERSION; return VERSION
